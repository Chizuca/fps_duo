﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 10f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    // Radius for the ground distance
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
        
    // Velocity of the player
    private Vector3 velocity;
    public bool isGrounded;
    
    //life player
    public float life;
    public TextMeshProUGUI lifeText;

    public GameObject deadPanel;
    // Update is called once per frame
    void Update()
    {
        lifeText.text = life + "/5";
        if (life <= 0)
        {
            Cursor.lockState = CursorLockMode.None;
            deadPanel.SetActive(true);
        }
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        // horizontal
        float x = Input.GetAxis("Horizontal");
        // vertical
        float z = Input.GetAxis("Vertical");
        
        // Move of the player
        Vector3 move = transform.right * x + transform.forward * z;
        
        // Move
        controller.Move(move * speed * Time.deltaTime);

        if (Input.GetButton("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
        
        velocity.y += gravity * Time.deltaTime;
        
        // Velocity
        controller.Move(velocity * Time.deltaTime);
    }
    
}
