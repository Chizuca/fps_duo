﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WeaponShoot : MonoBehaviour
{
    public float damage = 50f;
    public float range = 100f;
    public Camera fpsCam;
    public ParticleSystem shootFlash;
    public float impactForce = 30f;
    public int maxAmmo = 10, currentAmmo = -1;
    public float reloadTime = 1f;
    private bool isReloading;
    public Animator animator;
    public CameraShake cameraShake;
    public TextMeshProUGUI bulletText;

    private void Start()
    {
        //when the game start the loader are full
        currentAmmo = maxAmmo;
    }

    void Update()
    {
        bulletText.text = currentAmmo + "/" + maxAmmo;
        //if reload don't shoot if current amo = 0 go reload
        if (isReloading)
        {
            return;
        }
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }
        //if butoon mouse left down the weapon shoot
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        //start shake of weapon and animation of shoot start
        StartCoroutine(cameraShake.Shake(0.15f, 0.01f));
        shootFlash.Play();
        //the loader lost one bullet
        currentAmmo--;
        //using a raycast for detected a target
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit,range))
        {
            Debug.Log(hit.transform.name);
            Target target = hit.transform.GetComponent<Target>();
            Rat rat = hit.transform.GetComponent<Rat>();
            //if target touch is detected, he take damage 
            if (target != null)
            {
                target.TakeDamage(damage);
            }
            else if(rat != null)
            {
                rat.TakeDamage(true);
            }
            
            //the shoot add a force on object and propulse object
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
        }
    }

    IEnumerator Reload()
    {
        animator.SetBool("Reloading",true);
        isReloading = true;
        Debug.Log("reload");
        yield return new WaitForSeconds(reloadTime -0.25f);
        animator.SetBool("Reloading",false);
        yield return new WaitForSeconds(0.25f);
        isReloading = false;
        currentAmmo = maxAmmo;
    }
}
