﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatern : MonoBehaviour
{
    public float radiusVariable;
    public GameObject bullet , firePoint;
    public float timer;
    void Start()
    {
        
    }

    void Update()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radiusVariable);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Player"))
            {
                Shoot(hitCollider.gameObject);
            }
        }
        
    }

    void Shoot(GameObject player)
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Debug.Log("oui");
            timer = 2;
            LaunchProjectile();
        }
        transform.LookAt(player.transform);
        
    }
    void LaunchProjectile()
    {
        Instantiate(bullet , firePoint.transform.position,firePoint.transform.rotation);
    }
}
