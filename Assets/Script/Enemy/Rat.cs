﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Rat : MonoBehaviour
{
    public bool isAlive;
    public GameObject doorOpen;
    public GameObject doorClose;

    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive)
        {
            Die();
        }
    }

    // Call in WeaponShoot class
    public void TakeDamage(bool isTakeDamage)
    {
        isAlive = !isTakeDamage;
    }
    
    void Die()
    {
        // Open the door and destroy the rat
        doorClose.SetActive(false);
        doorOpen.SetActive(true);
        Destroy(gameObject);
    }
}
