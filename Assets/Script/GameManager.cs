﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject victory;
    // Update is called once per frame
    void Update()
    {
        // Check if all the enemies die
        CheckListEnemies();
    }

    void CheckListEnemies()
    {
        if (GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            Cursor.lockState = CursorLockMode.None;
            // if no more enemies -> Victory
            victory.SetActive(true);
        }
    }
}
