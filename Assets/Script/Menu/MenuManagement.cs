﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManagement : MonoBehaviour
{
    public int[] map;
    //random from map choice
    public void PlayButton()
    {
        Application.LoadLevel(Random.Range(1, map.Length));
    }
    //Button for quit this game
    public void QuitButton()
    {
        Application.Quit();
    }

    public void LoadMap(int map)
    {
        Application.LoadLevel(map);
    }
}
